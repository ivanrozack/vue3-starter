import { createRouter, createWebHistory } from 'vue-router'
import MainLayout from '../components/layouts/MainLayout.vue'
import AuthLayout from '../components/layouts/AuthLayout.vue'
import store from '../store'

const routes = [
  {
    path: '/',
    name: 'MainLayout',
    component: MainLayout,
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '../views/Main/Dashboard')
      },
      {
        path: 'document',
        name: 'Document',
        component: () => import(/* webpackChunkName: "document" */ '../views/Main/Document')
      },
      {
        path: 'user-management',
        name: 'UserManagement',
        component: () => import(/* webpackChunkName: "document" */ '../views/Main/UserManagement')
      },
      {
        path: 'transaction-history',
        name: 'History',
        component: () => import(/* webpackChunkName: "document" */ '../views/Main/History')
      },
      {
        path: 'settings',
        name: 'Settings',
        component: () => import(/* webpackChunkName: "settings" */ '../views/Main/Settings')
      }
    ],
    meta: { requiresAuth: true }
  },
  {
    path: '/auth',
    name: 'AuthLayout',
    component: AuthLayout,
    children: [
      {
        path: 'login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "login" */ '../views/Auth/Login.vue')
      },
      {
        path: 'signup',
        name: 'Signup',
        component: () => import(/* webpackChunkName: "signup" */ '../views/Auth/Registration.vue')
      },
      {
        path: 'forget-password',
        name: 'ForgetPass',
        component: () => import(/* webpackChunkName: "forgetPass" */ '../views/Auth/ForgetPassword.vue')
      }
    ],
    meta: { requiresVisitor: true }
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "notFound" */ '../views/NotFound.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next()
      return
    }
    next('auth/login')
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.isAuthenticated) {
      next('/')
      return
    }
    next()
  } else {
    next()
  }
})

export default router
