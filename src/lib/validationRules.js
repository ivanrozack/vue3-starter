const required = {
  required: true,
  message: 'Please fill out this field'
}

const email = {
  type: 'email',
  message: 'Please enter valid email.'
}

const number = {
  type: 'number',
  message: 'Field must be a number'
}

const minLength = (num) => {
  return { min: num, message: `Consist minimum ${num} character` }
}

const maxLength = (num) => {
  return { max: num, message: `Max length ${num} character` }
}

const emailPattern = {
  pattern: /^[a-zA-Z0-9@._-]*$/,
  message: 'Allowed characters: letters (a-z), numbers, underscores, periods, and dashes.'
}

const requiredWithMessage = (message) => {
  return { required: true, message }
}

const requireUpperLowercase = {
  required: true,
  pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[#?!@$%^&*-])(?=\S+$)/,
  message: 'Require at least uppercase and lowercase letter, numeric and special character'
}

const validationRules = {
  required,
  email,
  minLength,
  maxLength,
  number,
  emailPattern,
  requiredWithMessage,
  requireUpperLowercase
}

export default validationRules
