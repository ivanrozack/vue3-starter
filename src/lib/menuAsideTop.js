export default [
  {
    title: 'Dashboard',
    icon: require('@/assets/icons/nav_home.svg'),
    path: '/'
  },
  {
    title: 'Document',
    icon: require('@/assets/icons/nav_document.svg'),
    path: '/document'
  },
  {
    title: 'Users',
    icon: require('@/assets/icons/nav_users.svg'),
    path: '/user-management'
  },
  {
    title: 'Transaction History',
    icon: require('@/assets/icons/nav_history.svg'),
    path: '/transaction-history'
  }
]
