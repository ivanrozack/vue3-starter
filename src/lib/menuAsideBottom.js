export default [
  {
    title: 'Help Center',
    icon: require('@/assets/icons/nav_helpcenter.svg'),
    path: '/settings'
  },
  {
    title: 'Setting',
    icon: require('@/assets/icons/nav_settings.svg'),
    path: '/settings'
  },
  {
    title: 'Log Out',
    icon: require('@/assets/icons/nav_signout.svg'),
    path: '/logout'
  }
]
