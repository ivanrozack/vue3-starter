import apiClient from '@/services/signItApi'
import api from '../../lib/api'

export default {
  namespaced: true,
  state: {
    profile: null
  },
  mutations: {
    SET_USER_PROFILE: (state, data) => {
      state.profile = data
    }
  },
  actions: {
    getUserProfile(vuexCtx) {
      return new Promise((resolve, reject) => {
        apiClient.get(api.auth.user)
          .then((res) => {
            vuexCtx.commit('SET_USER_PROFILE', res.data)
            resolve(res.data)
          }).catch((err) => {
            reject(err)
          })
      })
    }
  }
}
