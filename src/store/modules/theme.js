import apiClient from '@/services/signItApi'
import api from '@/lib/api'
import defaultTheme from '@/lib/defaultTheme'

export default {
  namespaced: true,
  state: {
    value: null,
    lastSyncAt: null
  },
  mutations: {
    SET_THEME: (state, { theme, lastSyncAt, mergeToDefault = false }) => {
      let newTheme = theme
      if (mergeToDefault) {
        newTheme = mergeObjects(newTheme, defaultTheme)
      }
      state.value = newTheme
      state.lastSyncAt = lastSyncAt
    },
    SET_DEFAULT_THEME: (state) => {
      state.value = defaultTheme
      state.lastSyncAt = null
    }
  },
  actions: {
    async init(ctx) {
      const params = {
        domainName: window.location.hostname
      }

      const onError = () => {
        // Commit defaultTheme if only existing theme is absent.
        // Otherwise do nothing to keep using the existing.
        if (!ctx.state.value) {
          ctx.commit('SET_DEFAULT_THEME')
        }
      }

      const fetchPromise = apiClient
        .get(api.theme, { params })
        .then((res) => {
          ctx.commit('SET_THEME', {
            theme: res.data,
            lastSyncAt: new Date(),
            mergeToDefault: true
          })
        })
        .catch((err) => {
          console.error(err)
          onError()
        })

      const timeoutMs = 5000
      const onTimeout = () => {
        console.warn(`Fetching theme takes too long! (Limit: ${timeoutMs}ms)`)
        onError()
      }

      await executeWithTimeout(fetchPromise, timeoutMs, onTimeout)
    }
  },
  getters: {
    colors: (state) => state.value.colors,
    bgFront: (state) => state.value.images.bgFront,
    bgComp1: (state) => state.value.images.bgComp1,
    logoMain: (state) => state.value.images.logoMain,
    logoMark: (state) => state.value.images.logoMark,
    logoMarkWhite: (state) => state.value.images.logoMarkWhite
  }
}

const executeWithTimeout = async (executor, timeout, onTimeout) => {
  let timeoutId
  const timeoutPromise = new Promise((resolve) => {
    timeoutId = setTimeout(() => {
      onTimeout()
      resolve()
    }, timeout)
  })
  await Promise.race([executor, timeoutPromise])
  clearTimeout(timeoutId)
}

// Copy all present fields in defaultObj to absent fields in targetObj
function mergeObjects(targetObj, defaultObj) {
  function isObject(value) {
    return typeof value === 'object' && value !== null
  }

  for (let key in targetObj) {
    if (Object.prototype.hasOwnProperty.call(targetObj, key)) {
      if (targetObj[key] === null || targetObj[key] === undefined) {
        if (Object.prototype.hasOwnProperty.call(defaultObj, key)) {
          if (isObject(defaultObj[key])) {
            targetObj[key] = mergeObjects({}, defaultObj[key])
          } else {
            targetObj[key] = defaultObj[key]
          }
        }
      } else if (isObject(targetObj[key])) {
        targetObj[key] = mergeObjects(targetObj[key], defaultObj[key] || {})
      }
    }
  }

  for (let key in defaultObj) {
    if (
      Object.prototype.hasOwnProperty.call(defaultObj, key) &&
      !Object.prototype.hasOwnProperty.call(targetObj, key)
    ) {
      if (isObject(defaultObj[key])) {
        targetObj[key] = mergeObjects({}, defaultObj[key])
      } else {
        targetObj[key] = defaultObj[key]
      }
    }
  }

  return targetObj
}
