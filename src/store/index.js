import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'
import User from './modules/user'
import Theme from './modules/theme'
import apiClient from '@/services/signItApi'
import api from '@/lib/api'

const ls = new SecureLS({ encodingType: 'aes', isCompression: false })

export default createStore({
  state: {
    authentication: null
  },
  mutations: {
    SET_AUTHENTICATION: (state, data) => {
      state.authentication = data
    },
    REMOVE_CACHED_DATA: (state) => {
      state.authentication = null
    }
  },
  actions: {
    login(ctx, data) {
      apiClient
        .post(api.auth.login, data)
        .then((res) => {
          console.log('res', res)
        })
        .catch((err) => {
          console.log('err', err)
        })
      ctx.commit('SET_AUTHENTICATION', data)
    },
    logout(ctx) {
      ctx.commit('REMOVE_CACHED_DATA')
    }
  },
  getters: {
    isAuthenticated(state) {
      return state.authentication !== null
    }
  },
  modules: {
    user: User,
    theme: Theme
  },
  plugins: [
    createPersistedState({
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key)
      },
      paths: ['authentication', 'theme.value', 'theme.lastSyncAt']
    })
  ]
})
