import { useCssVar } from '@vueuse/core'
import tinycolor from 'tinycolor2'

const applyTheme = async (store) => {
  try {
    const theme = await getTheme(store)
    applyColors(theme)
    replaceElementUiTheme(theme)
  } catch (err) {
    console.error(err)
  }
}

const applyColors = (theme) => {
  const colors = theme.colors

  const primary = useCssVar('--app-color-primary')
  const secondary = useCssVar('--app-color-secondary')
  const tertiary = useCssVar('--app-color-tertiary')
  const primaryLight = useCssVar('--app-color-primary-light')
  const tertiaryLight = useCssVar('--app-color-tertiary-light')

  primary.value = colors.primary
  secondary.value = colors.secondary
  tertiary.value = colors.tertiary
  primaryLight.value = tinycolor(colors.primary).brighten(15).toString()
  tertiaryLight.value = tinycolor(colors.tertiary).brighten(25).toString()
}

const getTheme = async (store) => {
  const theme = store.state.theme

  if (!theme.value || isThemeExpired(theme)) {
    await store.dispatch('theme/init')
  }

  return store.state.theme.value
}

const isThemeExpired = (theme) => {
  const maxAgeHour = 1

  try {
    let ageHour = theme.lastSyncAt
      ? (new Date().getTime() - new Date(theme.lastSyncAt).getTime()) /
        (1000 * 60 * 60)
      : null
    return ageHour === null || ageHour > maxAgeHour
  } catch (err) {
    console.error(err)
    return true
  }
}

const replaceElementUiTheme = (theme) => {
  const colors = theme.colors
  const elPrimary = useCssVar('--el-color-primary')
  elPrimary.value = colors.primary

  for (let i = 1; i <= 9; i++) {
    const lighter = useCssVar('--el-color-primary-light-' + i)
    const brightAmount = i * 5
    lighter.value = tinycolor(colors.primary).brighten(brightAmount).toString()

    const darker = useCssVar('--el-color-primary-dark-' + i)
    const darkAmount = i * 6
    darker.value = tinycolor(colors.primary).darken(darkAmount).toString()
  }
}

export { applyTheme }
