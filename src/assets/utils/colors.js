module.exports = {
  success: '#0EC300',
  warning: '#FFC107',
  danger: '#FF0000',
  darkGrey: '#4D4D4D',
  grey: '#767676',
  greyLight: '#CED4DA',
  black: '#000000',
  white: '#FFFFFF'
}
