import axios from 'axios'
import store from '@/store'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_SIGN_IT_API_URL
})

apiClient.interceptors.request.use(
  (request) => {
    const token = store.state.authentication?.token
    if (token) {
      request.headers.Authorization = `Bearer ${token}`
    }
    return request
  },
  (error) => {
    return Promise.reject(error)
  }
)

apiClient.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    // handle error not authorized and etc here
    return Promise.reject(error)
  }
)

const buildUrl = (path) => {
  return `${apiClient.getUri()}/${path}`
}

export default apiClient
export { buildUrl }
