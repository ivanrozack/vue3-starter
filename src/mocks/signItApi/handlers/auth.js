import { rest } from 'msw'
import { buildUrl } from '@/services/signItApi'
import api from '@/lib/api'

const login = rest.post(buildUrl(api.auth.login), async (req, res, ctx) => {
  const { email, password } = await req.json()

  if (email === 'budi@gmail.com' && password === 'aassdd') {
    return res(
      ctx.status(200),
      ctx.json({
        token: 'abcdef'
      })
    )
  }

  return res(ctx.status(400))
})

export default [login]
