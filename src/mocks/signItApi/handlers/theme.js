import { rest } from 'msw'
import { buildUrl } from '@/services/signItApi'
import api from '@/lib/api'

const theme = rest.get(buildUrl(api.theme), async (req, res, ctx) => {
  const domainName = req.url.searchParams.get('domainName')
  const file = domainName === 'localhost' ? 'theme_1' : 'theme_2'
  const data = require(`../fixtures/${file}.json`)
  const fakeLoadingTime = 1500

  await new Promise((resolve) => setTimeout(resolve, fakeLoadingTime))

  return res(ctx.status(200), ctx.json(data))
})

export default [theme]
