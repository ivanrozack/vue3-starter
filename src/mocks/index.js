const isEnabled =
  process.env.NODE_ENV === 'development' &&
  process.env.VUE_APP_ENABLE_MOCKS === 'true'

const mockIfEnabled = async () => {
  if (!isEnabled) {
    return
  }

  const signApiHandlers = require('./signItApi/handlers')
  const { setupWorker } = require('msw')

  const worker = setupWorker(...signApiHandlers.default)
  await worker.start({
    onUnhandledRequest: 'bypass'
  })
}

export { mockIfEnabled }
