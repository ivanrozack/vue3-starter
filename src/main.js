import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import { mockIfEnabled } from './mocks'
import './assets/scss/main.scss'
import ElementPlus from 'element-plus'

const mountApp = async () => {
  await mockIfEnabled()
  app.mount('#app')
}

const app = createApp(App)
app.use(router)
app.use(store)
app.use(ElementPlus)

mountApp()
