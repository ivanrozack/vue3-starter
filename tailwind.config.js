const colors = require('./src/assets/utils/colors')

module.exports = {
  important: true,
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: 'var(--app-color-primary)',
        secondary: 'var(--app-color-secondary)',
        tertiary: 'var(--app-color-tertiary)',
        success: colors.success,
        warning: colors.warning,
        danger: colors.danger,
        darkGrey: colors.darkGrey,
        grey: colors.grey,
        greyLight: colors.greyLight,
        black: colors.black,
        white: colors.white
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: [require('@tailwindcss/container-queries')]
}
